import actions from '../actions';
import { NUM_OF_TILES, MAX_CHANCES } from '../config';
import GAME_STATUS from '../utils/gameStatus';
import { getOrderedNumbersArray } from '../utils/number';

export const initialState = {
  chosenNumber: -1,
  chancesLeft: MAX_CHANCES,
  gameStatus: GAME_STATUS.CHOOSE_NUMBER,
  tilesNumbers: getOrderedNumbersArray(NUM_OF_TILES),
};

export const reducer = (state, action) => {
  const { type, payload } = action;

  switch (type) {
    case actions.CHOOSE_NUMBER:
      return {
        ...state,
        gameStatus: GAME_STATUS.PLAYING,
        chosenNumber: payload.chosenNumber,
        tilesNumbers: payload.tilesNumbers,
      };
    case actions.GUESS_NUMBER:
      if (state.chosenNumber === payload.number) {
        return { ...state, gameStatus: GAME_STATUS.WON };
      }

      if (state.chancesLeft === 1) {
        return { ...state, gameStatus: GAME_STATUS.LOST };
      }

      return { ...state, chancesLeft: state.chancesLeft - 1 };
    case actions.RESET_GAME:
      return initialState;
    default:
      throw new Error();
  }
};
