const actions = {
  CHOOSE_NUMBER: 'CHOOSE_NUMBER',
  GUESS_NUMBER: 'GUESS_NUMBER',
  RESET_GAME: 'RESET_GAME',
};

export default actions;
