export const theme = {
  primaryColor1: '#00d09c',
  primaryColor2: '#5367ff',
  textColor: '#444444',

  fullEmphasis: '#000000',
  highEmphasis: '#1f242a',
  midEmphasis: '#333333',
  lowEmphasis: '#CECECE',
  zeroEmphasis: '#FFFFFF',
};
