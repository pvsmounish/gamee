import React from 'react';

import { View, Text, StyleSheet } from 'react-native';

import { colors } from '../../styles';

const ChancesLeftBox = ({ chancesLeft }) => (
  <View style={styles.container}>
    <Text style={styles.chancesLeftText}>{chancesLeft}</Text>
    <Text style={styles.chancesLeftHeading}>
      {chancesLeft > 1 ? 'Chances Left' : 'Chance Left'}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.theme.primaryColor2,
    margin: 15,
    padding: 15,
    borderRadius: 4,
    alignItems: 'center',
  },
  chancesLeftHeading: {
    color: colors.theme.zeroEmphasis,
    fontSize: 25,
  },
  chancesLeftText: {
    color: colors.theme.zeroEmphasis,
    fontWeight: '800',
    fontSize: 35,
  },
});

export default ChancesLeftBox;
