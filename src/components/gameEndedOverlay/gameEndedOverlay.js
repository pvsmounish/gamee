import React from 'react';

import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { colors } from '../../styles';

const GameEndedOverlay = ({ gameStatusText, playAgainAction }) => (
  <View style={styles.container}>
    <Text style={styles.heading}>{'Game Ended'.toUpperCase()}</Text>
    <Text style={styles.statusText}>{gameStatusText}</Text>
    <TouchableOpacity onPress={playAgainAction}>
      <Text style={styles.playAgainText}>Play Again</Text>
    </TouchableOpacity>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    left: 0,
    top: 0,
    backgroundColor: `${colors.theme.midEmphasis}90`,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  heading: {
    fontSize: 25,
    color: colors.theme.zeroEmphasis,
  },
  statusText: {
    fontSize: 35,
    color: colors.theme.zeroEmphasis,
  },
  playAgainText: {
    color: colors.theme.zeroEmphasis,
  },
});

export default GameEndedOverlay;
