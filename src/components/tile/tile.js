import React from 'react';

import { Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';

import { colors } from '../../styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const TILE_SIZE = (WINDOW_WIDTH - 90) / 3; // (margin left + margin right)*3 -> (15 + 15)*3 = 90

const Tile = ({ number = 3, onPress, disabled, color }) => (
  <TouchableOpacity
    style={{ ...styles.container, backgroundColor: color ?? colors.theme.primaryColor1 }}
    onPress={onPress}
    disabled={disabled}
  >
    <Text style={styles.number}>{number}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    width: TILE_SIZE,
    height: TILE_SIZE,
    marginHorizontal: 15,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
  },
  number: {
    color: colors.theme.zeroEmphasis,
    fontSize: 25,
  },
});

export default Tile;
