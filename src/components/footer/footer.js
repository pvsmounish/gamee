import React from 'react';

import { View, Text, StyleSheet } from 'react-native';

import { colors } from '../../styles';

const Footer = () => (
  <View style={styles.container}>
    <Text style={styles.infoText}>
      Made with ❤️ for <Text style={styles.boldText}>Groww</Text>
    </Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 5,
  },
  infoText: {
    fontSize: 18,
    color: colors.theme.textColor,
  },
  boldText: {
    fontWeight: 'bold',
  },
});

export default Footer;
