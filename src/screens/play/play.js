import React, { useEffect, useReducer, useState } from 'react';

import { View, StyleSheet, Text, Image, ScrollView } from 'react-native';

import actions from '../../actions';
import { Tile, Footer, ChancesLeftBox, GameEndedOverlay } from '../../components';
import { MAX_CHANCES, NUM_OF_TILES } from '../../config';
import { initialState, reducer } from '../../reducers';
import { colors } from '../../styles';
import GAME_STATUS from '../../utils/gameStatus';
import { getShuffledNumbersArray } from '../../utils/number';
import { showToast } from '../../utils/toasts';

export const APP_LOGO_LOCATION = '../../assets/images/gamee-logo.png';

const PlayScreen = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const { chancesLeft, gameStatus, tilesNumbers } = state;

  useEffect(() => {
    if (chancesLeft > 0 && chancesLeft < MAX_CHANCES) {
      showToast('Please try again!');
    }
  }, [chancesLeft]);

  const chooseNumber = (number) => {
    dispatch({
      type: actions.CHOOSE_NUMBER,
      payload: { chosenNumber: number, tilesNumbers: getShuffledNumbersArray(NUM_OF_TILES) },
    });
  };

  const pressTile = (number) => {
    dispatch({ type: actions.GUESS_NUMBER, payload: { number } });
  };

  const pressPlayAgain = () => {
    dispatch({ type: actions.RESET_GAME });
  };

  return (
    <>
      <ScrollView style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={require(APP_LOGO_LOCATION)} style={styles.logo} />
        </View>

        <View style={styles.headingContainer}>
          <Text style={styles.heading}>
            {gameStatus === GAME_STATUS.CHOOSE_NUMBER ? 'Select a Number' : 'Guess the Number'}
          </Text>
        </View>

        {gameStatus === GAME_STATUS.CHOOSE_NUMBER && (
          <View style={styles.tilesContainer}>
            {tilesNumbers.map((number) => (
              <Tile
                number={number}
                onPress={() => chooseNumber(number)}
                key={number}
                color={colors.theme.primaryColor2}
              />
            ))}
          </View>
        )}

        {[GAME_STATUS.PLAYING, GAME_STATUS.WON, GAME_STATUS.LOST].includes(gameStatus) && (
          <View style={styles.tilesContainer}>
            {tilesNumbers.map((number) => (
              <PlayingTile number={number} onPress={() => pressTile(number)} key={number} />
            ))}
          </View>
        )}

        {gameStatus === GAME_STATUS.PLAYING && <ChancesLeftBox chancesLeft={chancesLeft} />}
      </ScrollView>

      <Footer />

      {(gameStatus === GAME_STATUS.WON || gameStatus === GAME_STATUS.LOST) && (
        <GameEndedOverlay
          gameStatusText={gameStatus === GAME_STATUS.WON ? 'You Won!' : 'You Lost!'}
          playAgainAction={pressPlayAgain}
        />
      )}
    </>
  );
};

// Show the Tile number only after tap
const PlayingTile = ({ number, onPress }) => {
  const [isTapped, setTapped] = useState(false); // TODO improve state name

  const onPressTile = () => {
    setTapped(true);
    onPress();
  };

  return (
    <Tile
      style={styles.tile}
      color={isTapped ? colors.theme.primaryColor2 : colors.theme.primaryColor1}
      number={isTapped ? number : 'X'}
      onPress={onPressTile}
      disabled={isTapped}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.theme.zeroEmphasis,
    paddingTop: 10,
    marginBottom: 60,
  },
  logoContainer: {
    alignItems: 'center',
  },
  logo: {
    height: 100,
    width: 200,
  },
  headingContainer: {
    paddingHorizontal: 15,
  },
  heading: {
    fontSize: 25,
    fontWeight: '700',
    textAlign: 'left',
    color: colors.theme.textColor,
  },
  tilesContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

export default PlayScreen;
