const gameStatus = {
  CHOOSE_NUMBER: 'GAME_CHOOSE_NUMBER',
  PLAYING: 'GAME_PLAYING',
  WON: 'GAME_WON',
  LOST: 'GAME_LOST',
};

export default gameStatus;
