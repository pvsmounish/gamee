export const getShuffledNumbersArray = (number) => {
  return shuffleArray(getOrderedNumbersArray(number));
};

export const getOrderedNumbersArray = (number) => {
  return [...Array(number + 1).keys()].slice(1);
};

const shuffleArray = (array) => {
  let currentIndex = array.length;
  let randomIndex;

  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
  }

  return array;
};
