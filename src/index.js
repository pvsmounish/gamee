import React from 'react';

import { PlayScreen } from './screens/play';

const App = () => <PlayScreen />;

export default App;
